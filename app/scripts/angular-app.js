"use strict";

var colorAdminApp = angular.module('colorAdminApp', [
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'firebase'
    ]);

colorAdminApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/app');

    $stateProvider
    .state('app', {
        url: '/app',
        templateUrl: 'template/app.html',
        controller: 'homeController',
        resolve: {
            service: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'colorAdminApp',
                    serie: true,
                    files: [
                    'scripts/controllers/home-controller.js',
                    ] 
                });
            }]
        }
    })
    .state('locals', {
        url: '/locals',
        templateUrl: 'template/locals-view.html'
    })
    .state('exploreSports', {
        url: '/explore-sports',
        templateUrl: 'template/explore-sports.html',
        controller: 'exploreSportsController',
        resolve: {
            service: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'colorAdminApp',
                    serie: true,
                    files: [
                        'scripts/controllers/explore-sports-controller.js',
                    ] 
                });
            }]
        }

    })
    .state('sports', {
        url: '/sports/:name',
        templateUrl: 'template/sports-view.html',
        controller: 'sportsController',
        resolve: {
            service: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'colorAdminApp',
                    serie: true,
                    files: [
                        'scripts/controllers/sports-controller.js',
                    ] 
                });
            }]
        }
    })
    .state('addSport', {
        url: '/addSport',
        templateUrl: 'template/add-sport-view.html',
        controller: 'AddSportController',
        resolve: {
            service: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'colorAdminApp',
                    serie: true,
                    files: [
                    'scripts/controllers/add-sport-controller.js',
                    ] 
                });
            }]
        }
    })

}]);

colorAdminApp.run(['$rootScope', '$state', 'setting', function($rootScope, $state, setting) {
    $rootScope.$state = $state;
    $rootScope.setting = setting;
}]);