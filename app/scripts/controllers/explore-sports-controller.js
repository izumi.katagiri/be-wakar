/* -------------------------------
CONTROLLER - Explore Sports
------------------------------- */
"use strict";

angular.module('colorAdminApp').controller('exploreSportsController',
	function($scope, $rootScope, $state, $uibModal, $firebaseObject) {

		$scope.sports = [];

		var ref = firebase.database().ref('sports');
		var query = ref.orderByChild('name').limitToFirst(10);
        var data = {};

        ref.on('value', snap => {
            var object = snap.val();
            var sports = [];

            var keys = Object.keys(object);

            for (var i = keys.length - 1; i >= 0; i--) {
            	var key = keys[i];
            	var sport = object[key];

            	sports.push(sport);
            }

            console.log($scope.sports);

			$scope.$apply(function(){
			  $scope.sports = sports;
			})
        });
		

		/********** Funções que rodam por último **********/

		angular.element(document).ready(function () {
			App.initPage();
		});
	}
);
