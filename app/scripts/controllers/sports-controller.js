/* -------------------------------
CONTROLLER - Sports
------------------------------- */
"use strict";

angular.module('colorAdminApp').controller('sportsController',
	function($scope, $rootScope, $state, $uibModal, $firebaseObject, $timeout ) {

		var name = $state.params.name;
		var modalInstance = null;

		/********** Funções dos modais **********/

		var openInfos = function(type){
			var size = 'lg';
			var templateUrl = (type == 'location') ? 'assets/templates/modal-location-info.html' : 'assets/templates/modal-info.html';

			modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: templateUrl,
				size: size,
				scope: $scope
			});
		};

		var closeModal = function(){
			modalInstance.dismiss('cancel');
		};

		var initLocations = function(object){
			var locations = [];
			var keys = _.keys(object);

			for (var i = keys.length - 1; i >= 0; i--) {
				var location = object[keys[i]];

				locations.push(location);
			};

			return locations;
		};

		var locationInfo = function(location){
			$scope.selectedLocation = location;
			openInfos('location');
		};

		/********** Funções JQuery **********/

		var normalizeHeight = function(){
			var target = '.info-card';
			var maxHeight = 0;

			$(target).each(function(){
				if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
			});

			$(target).each(function(){
				$(this).height(maxHeight);
			});
		};

		/********** Funções que interagem com a view **********/

		$scope.functions = {
			openInfos: openInfos,
			locationInfo: locationInfo,
			closeModal: closeModal,
		};

		$scope.bgImg = 'assets/img/bg/' + name + '.jpg';

		/********** Query **********/

		var ref = firebase.database().ref('sports');
        var query = ref.orderByChild('name').equalTo('Airsoft').limitToFirst(1);
        var data = {};
        var sportKeyName = null;

        var locationsRef =  firebase.database().ref('locations');

        query.on('value', snap => {
            var object = snap.val();

            sportKeyName = _.head(_.keys(object));

            data = object[sportKeyName];
            var locations = [];

            var queryLocations = locationsRef.orderByChild('sportKey').equalTo(sportKeyName);

            queryLocations.on('value', snap => {
            	var locationSnap = snap.val();
            	locations = initLocations(locationSnap);

				$scope.$apply(function(){
					$scope.view = data;
					$scope.locations = locations;

					console.log(locations[0]);

					$timeout(function(){
						normalizeHeight();
					},10);
				});
            });


        });
		

		/********** Funções que rodam por último **********/

		angular.element(document).ready(function () {
			App.initPage();
			$('header.masthead').css('background-image','url('+$scope.bgImg+')');
		});
	}
);


//Equipamentos:As armas podem ser alugadas no local, geralmente é possível escolher diferentes tipos: pistola, rifle, etc...;Muitos locais oferecem macacão, colete e luvas para alugar.;Todos oferecem o equipamento mínimo de proteção, máscaras ou óculos./Esforço:Lembre-se, você estará uma guerra. É um confronto, então você terá de correr, se abaixar, pular e usar muita estratégia para vencer o confronto./Treinamento:Não é necessário ter nenhum tipo de treinamento prévio, todas as explicações são fornecidas pelos locais./Restrições:Ter ao menos 12 anos de idade./Dicas:Os tiros de bolinha não doem muito, mas é aconselhável ir com roupas que cubram o corpo inteiro e que sejam maleáveis.;Leve uma garrafa de água.

//Preparado para entrar em uma guerra? Eliminar todos os seus adversários ou entrar na zona inimiga para salvar um refém?/Estas são algumas das missões que você vai encontrar jogando Airsoft. Uma simulação de combate utilizando armas que disparam bolinhas de plástico.