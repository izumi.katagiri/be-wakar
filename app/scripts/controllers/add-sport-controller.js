/* -------------------------------
CONTROLLER - Sports
------------------------------- */
"use strict";

angular.module('colorAdminApp').controller('AddSportController', function($scope, $rootScope, $state, $uibModal, $q, $firebaseObject) {

	/********** Declaração das funções **********/

	var getSlashIndex = function(string){
		var re = /\//g;

		var index = string.search(re);

		return index;
	};

	var getColumnIndex = function(string){
		var re = /:/g;

		var index = string.search(re);

		return index;
	};

	var getSemicolumnIndex = function(string){
		var re = /;/g;

		var index = string.search(re);

		return index;
	};

	var getParagraphFromDescription = function(string){
		var paragraphs = [];
		var index = getSlashIndex(string);

		while(index > -1){
			var paragraph = string.slice(0, index);
			string = string.substring(index+1);
			index = index = getSlashIndex(string);

			paragraphs.push(paragraph);
		}

		paragraphs.push(string);

		return paragraphs;
	};

	var getTopicsFromInfos = function(string){
		var topics = [];
		var index = getSlashIndex(string);

		while(index > -1){
			var topic = string.slice(0, index);
			string = string.substring(index+1);
			index = index = getSlashIndex(string);

			topics.push(topic);
		}

		topics.push(string);

		return topics;
	};

	var getTitleTextFromTopic = function(string){
		var index = getColumnIndex(string);
		var result = [];
		if (index > -1) {
			var title = string.slice(0, index);
			var text = string.substring(index+1);

			result.title = title;
			result.text = text;

			return result;
		} else {
			alert('Sintaxe está errada');
			return null;
		};
	};

	var getItemFromText = function(string){
		var items = [];
		var index = getSemicolumnIndex(string);

		while(index > -1){
			var item = string.slice(0, index);
			string = string.substring(index+1);
			index = getSemicolumnIndex(string);

			items.push(item);
		}

		items.push(string);

		return items;
	};

	var submit = function(){
		var descriptionParagraphs = getParagraphFromDescription($scope.data.description);
		var infoTopics = getTopicsFromInfos($scope.data.infos);
		var name = $scope.data.sportName;
		var smallDescription = $scope.data.smallDescription;

		for (var i = infoTopics.length - 1; i >= 0; i--) {
			var topic = getTitleTextFromTopic(infoTopics[i]);
			topic.text = getItemFromText(topic.text);

			var title = topic.title;
			var text = topic.text;
			var obj = {
				title: title,
				text: text
			};

			infoTopics[i] = obj;
		}

		var postData = {
			name: name,
			smallDescription: smallDescription,
			description: descriptionParagraphs,
			infos: infoTopics
		};

		console.log(postData);
		var newPostKey = firebase.database().ref().child('sports').push().key;

		var updates = {};
		updates['/sports/' + newPostKey] = postData;

		var post = firebase.database().ref().update(updates);

		post.then(function(response){
			console.log(response);
		});

		post.catch(function(error){
			console.log(error);
		});

	};

	/********** Funções para location **********/

	var getItemsBySemicolumn = function(string){
		var index = getSemicolumnIndex(string);
		var array = [];

		while(index > -1){
			var item = string.slice(0, index);
			string = string.substring(index+1);
			index = getSemicolumnIndex(string);

			array.push(item);
		}

		array.push(string);

		return array;
	};

	var location = {
		convert: function(str){
			var reti = /\[/;
			var reto = /\]/;
			var resti = /\(/;
			var resto = /\)/;
			var semicolumn = /;/;

			var title = str.substring(str.search(reti) + 1, str.search(reto));
			var subtitle = str.substring(str.search(resti) + 1, str.search(resto));
			var items = getItemsBySemicolumn(str.substring(str.search(resto) + 1));

			var object = {
				title: title,
				subtitle: subtitle,
				items: items
			};

			return object;
		}
	}

	var getForeignKey = function(sportName){
		var deferred = $q.defer();
		
		var ref = firebase.database().ref('sports');
        var query = ref.orderByChild('name').equalTo('Airsoft').limitToFirst(1);
        var key = null;

        query.on('value', snap => {
            var object = snap.val();

            var keys = Object.keys(object);
            var key = keys[0];

            deferred.resolve(key);
        });

        return deferred.promise;
	};

	var getOperatingHours = function(string){
		var index = getSlashIndex(string);

		var array = [];

		while(index > -1){
			var item = string.slice(0, index);
			string = string.substring(index+1);
			index = getSlashIndex(string);

			array.push(item);
		}

		array.push(string);

		return array;
	};


	var submitLocation = function(){
		var promise = getForeignKey($scope.location.sportName);

		promise.then(function(response){
			var foreignKey = response;
			var infoTopics = getTopicsFromInfos($scope.location.infos);
			var convertedTopics = [];
			for (var i = infoTopics.length - 1; i >= 0; i--) {
				var topic = infoTopics[i];
				var convertedTopic = location.convert(topic);
				convertedTopics.push(convertedTopic);
			};

			var operatingHours = getOperatingHours($scope.location.operatingHours);

			var postData = {
				sportKey: foreignKey,
				name: $scope.location.name,
				rating: $scope.location.rating,
				address: $scope.location.address,
				operatingHours: operatingHours,
				price: $scope.location.price,
				phone: $scope.location.phone,
				infos: convertedTopics
			};

			var newPostKey = firebase.database().ref().child('locations').push().key;

			var updates = {};
			updates['/locations/' + newPostKey] = postData;

			var post = firebase.database().ref().update(updates);

			post.then(function(){
				$state.reload();
			});

			// post.catch(function(error){
			// 	console.log(error);
			// });
		});

		
	};
		

	/********** Instancia dos objetos do $scope **********/
	$scope.data = {
		sportName: '',
		smallDescription: '',
		description: '',
		infos: ''
	};

	$scope.location = {
		sportName: null,
		name: null,
		rating: null,
		address: null,
		operatingHours: null,
		price: null,
		phone: null,
		infos: null
	};

	$scope.functions={
		submit: submit,
		submitLocation: submitLocation
	};

	/********** Funções que rodam por último **********/

	angular.element(document).ready(function () {
		App.initPage();
	});
});


//[Cenário](Hospício abandonado)Campo 1 (4 a 10 pessoas) - Pistola;Campo 2 (8 a 26 pessoas) - Rifle/[Valores](Campo + arma (preço total))Campo 1 – R$60 por pessoa por 2 horas de jogo – munição ilimatada;Campo 2 – R$90 por pessoa por 2 horas de jogo – munição ilimatada;Pistolas*: Colt 1911, Sig Sauer P226, Glock e Desert Eagle;Rifles*: Colt M4A1, AK-47, FN FAL, Sig 556, HK MP5, AUG, P90, FN Scar e Espingarda Cal.12/[Outros equipamentos disponíveis]()Óculos, máscara, colete e luva – Incluso para campo + arma/[Forma de pagamento]()Dinheiro;Cartão de débito;Cartão de crédito/[Idade mínima 12 anos]()s